package basic.android.bookshare.model;

import java.io.Serializable;

public class BookModel implements Serializable {

    private String openLibraryId;
    private String author;
    private String title;

    public BookModel() {
    }

    public BookModel(String openLibraryId, String author, String title) {
        this.openLibraryId = openLibraryId;
        this.author = author;
        this.title = title;
    }

    public String getOpenLibraryId() {
        return openLibraryId;
    }

    public void setOpenLibraryId(String openLibraryId) {
        this.openLibraryId = openLibraryId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
