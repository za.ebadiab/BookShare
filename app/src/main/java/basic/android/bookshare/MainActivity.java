package basic.android.bookshare;

import android.app.ProgressDialog;
import android.support.v4.view.MenuItemCompat;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import basic.android.bookshare.model.BookModel;
import cz.msebera.android.httpclient.Header;

public class MainActivity extends BaseActivity {

    RecyclerView recyclerView;

    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bind();

    }

    public void bind() {
        recyclerView = findViewById(R.id.recyclerView);
        dialog = new ProgressDialog(mContext);
        dialog.setTitle("loading");
        dialog.setMessage("Please wait to load data from Open Library");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search_book, menu);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Fetch the data remotely
                searchBookListFromOpenlibrary(query);
                // Reset SearchView
                searchView.clearFocus();
                searchView.setQuery("", false);
                searchView.setIconified(true);
                searchItem.collapseActionView();
                // Set activity title to search query
                MainActivity.this.setTitle(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
        return true;
    }


    void searchBookListFromOpenlibrary(String bookTitle) {

        String book_search_URL = "http://openlibrary.org/search.json?title="
                + bookTitle;

        dialog.show();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(book_search_URL, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject responseJson) {

                checkOpenLibraryResponseForBookExists(responseJson);

            }

            @Override
            public void onFinish() {
                super.onFinish();
                dialog.dismiss();
            }


        });
    }


    void checkOpenLibraryResponseForBookExists(JSONObject responseJson) {

        if (responseJson.has("num_found")) {
            parseJson(responseJson);
        } else {
            Toast.makeText(mContext, "No book found!", Toast.LENGTH_LONG).show();
        }

    }

    public void parseJson(JSONObject responseJson) {
        ArrayList<BookModel> books = new ArrayList<>();
        try {
            JSONArray docsJsonArray = responseJson.getJSONArray("docs");
            int arrayLength;
            if (responseJson.getInt("num_found") > 10) {
                arrayLength = 20;
            } else {
                arrayLength = docsJsonArray.length();
            }

            for (int i = 0; i < arrayLength; i++) {
                JSONObject bookJson = null;
                try {
                    bookJson = docsJsonArray.getJSONObject(i);

                    BookModel bookModel = setBookModel(bookJson);
                    if (bookModel != null) {

                        books.add(bookModel);
                    }
                } catch (Exception e) {
                    Log.d("my log", "exception in parseJson= " + e.getClass());
                }
            }
            setAdapter(books);

        } catch (Exception e) {
            Log.d("my log", "exception in parseJson= " + e.getClass());
        }

    }

    public void setAdapter(ArrayList<BookModel> books) {
        RecyclerBookAdapter adapter = new RecyclerBookAdapter(mContext, books);

        recyclerView.addItemDecoration(new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(adapter);
    }

    public BookModel setBookModel(JSONObject bookJson) {
        BookModel bookModel = new BookModel();

        try {
            if (bookJson.has("cover_edition_key")) {
                bookModel.setOpenLibraryId(bookJson.getString("cover_edition_key"));
            } else if (bookJson.has("edition_key")) {
                bookModel.setOpenLibraryId(bookJson.getJSONArray("edition_key").getString(0));
            }
            bookModel.setTitle(bookJson.has("title_suggest") ? bookJson.getString("title_suggest") : "");
            bookModel.setAuthor(bookJson.getJSONArray("author_name").get(0).toString());

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        return bookModel;
    }

}
