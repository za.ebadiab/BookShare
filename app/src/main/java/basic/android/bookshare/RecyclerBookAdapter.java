package basic.android.bookshare;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import basic.android.bookshare.model.BookModel;

public class RecyclerBookAdapter extends RecyclerView.Adapter<RecyclerBookAdapter.Holder> {

    private Context mContext;
    private ArrayList<BookModel> bookModelArrayList;

    public RecyclerBookAdapter(Context mContext, ArrayList<BookModel> bookModelArrayList) {
        this.mContext = mContext;
        this.bookModelArrayList = bookModelArrayList;
    }


    @NonNull
    @Override
    public RecyclerBookAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(mContext).inflate(R.layout.book_items, parent, false);
        return new Holder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerBookAdapter.Holder holder, int position) {

        Picasso.with(mContext).load(Uri.parse(getSmallCoverUrl(bookModelArrayList.get(position).getOpenLibraryId()))).error(R.drawable.ic_nocover).into(holder.imgBookCover);

        holder.txtTitle.setText(bookModelArrayList.get(position).getTitle());
        holder.txtAuthor.setText(bookModelArrayList.get(position).getAuthor());

    }

    @Override
    public int getItemCount() {
        return bookModelArrayList.size();
    }


    public String getSmallCoverUrl(String openLibraryId) {
        return "http://covers.openlibrary.org/b/olid/" + openLibraryId + "-M.jpg?default=false";
    }


    class Holder extends RecyclerView.ViewHolder {
        ImageView imgBookCover;
        TextView txtTitle, txtAuthor;

        public Holder(View itemView) {
            super(itemView);

            imgBookCover = itemView.findViewById(R.id.imgBookCover);
            txtTitle = itemView.findViewById(R.id.txtTitle);
            txtAuthor = itemView.findViewById(R.id.txtAuthor);

            itemView.findViewById(R.id.relativeLayout).setOnClickListener(v -> {

                Intent intent = new Intent(mContext, ActivityBookDetails.class);
                intent.putExtra("bookModelObject", bookModelArrayList.get(getAdapterPosition()));
                mContext.startActivity(intent);
            });
        }


    }
}
