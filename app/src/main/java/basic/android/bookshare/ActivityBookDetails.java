package basic.android.bookshare;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import basic.android.bookshare.customViews.MyTextView;
import basic.android.bookshare.model.BookModel;
import cz.msebera.android.httpclient.Header;

public class ActivityBookDetails extends BaseActivity {
    ImageView imgBookCover;
    MyTextView txtTitle, txtAuthor, txtPublisher, txtPageCount;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_details);

        bind();
    }

    void bind() {
        imgBookCover = findViewById(R.id.imgBookCover);
        txtTitle = findViewById(R.id.txtTitle);
        txtAuthor = findViewById(R.id.txtAuthor);
        txtPublisher = findViewById(R.id.txtPublisher);
        txtPageCount = findViewById(R.id.txtPageCount);

        dialog = new ProgressDialog(mContext);
        dialog.setTitle("loading");
        dialog.setMessage("Please wait to load detal data from Open Library");

        // Use the book to populate the data into our views
        BookModel bookModel = (BookModel) getIntent().getSerializableExtra("bookModelObject");
        setBookAndGetDetailsFromOpenLibrary(bookModel);
    }


    private void setBookAndGetDetailsFromOpenLibrary(BookModel bookModel) {
        //change activity title
        this.setTitle(bookModel.getTitle());
        // Populate data
        Picasso.with(this).load(Uri.parse(getLargeCoverUrl(bookModel.getOpenLibraryId()))).error(R.drawable.ic_nocover).into(imgBookCover);
        txtTitle.setText(bookModel.getTitle());
        txtAuthor.setText(bookModel.getAuthor());

        // fetch extra book data from books API
        String book_detail_URL = "http://openlibrary.org/books/"
                + bookModel.getOpenLibraryId()
                + ".json";

        dialog.show();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(book_detail_URL, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject responseJson) {

                checkOpenLibraryResponseForBookDetailExists(responseJson);

            }


            @Override
            public void onFinish() {
                super.onFinish();
                dialog.dismiss();
            }


        });
    }

    void checkOpenLibraryResponseForBookDetailExists(JSONObject responseJson) {

        if (responseJson.has("publishers")) {
            parseJson(responseJson);
        } else {
            Toast.makeText(mContext, "No book detail found!", Toast.LENGTH_LONG).show();
        }
    }

    public void parseJson(JSONObject responseJson) {

        try {

            // display comma separated list of publishers
            final JSONArray publisher = responseJson.getJSONArray("publishers");
            final int numPublishers = publisher.length();
            final String[] publishers = new String[numPublishers];
            for (int i = 0; i < numPublishers; ++i) {
                publishers[i] = publisher.getString(i);
            }
            txtPublisher.setText(TextUtils.join(", ", publishers));

            if (responseJson.has("number_of_pages")) {
                String pageNumber = responseJson.getInt("number_of_pages") + " pages";
                txtPageCount.setText(pageNumber);
            }
        } catch (JSONException e) {
            Log.d("my log", "exception in parseJson BookDetail= " + e.getClass());
        }
    }


    public String getLargeCoverUrl(String openLibraryId) {
        return "http://covers.openlibrary.org/b/olid/" + openLibraryId + "-L.jpg?default=false";
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_book_detail_share, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_share) {
            setShareIntent();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void setShareIntent() {
        Uri bmpUri = getLocalBitmapUri(imgBookCover);
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.setType("*/*");
        shareIntent.putExtra(Intent.EXTRA_TEXT, (String) txtTitle.getText());
        shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);

        startActivity(Intent.createChooser(shareIntent, "Share Image"));
    }

    // Returns the URI path to the Bitmap displayed in cover imageview
    public Uri getLocalBitmapUri(ImageView imageView) {
        // Extract Bitmap from ImageView drawable
        Drawable drawable = imageView.getDrawable();
        Bitmap bmp = null;
        if (drawable instanceof BitmapDrawable) {
            bmp = ((BitmapDrawable) drawable).getBitmap();
        } else {
            return null;
        }
        // Store image to default external storage directory
        Uri bmpUri = null;
        try {
            File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                    "share_image_" + System.currentTimeMillis() + ".png");
            file.getParentFile().mkdirs();
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

}
